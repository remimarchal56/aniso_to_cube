[[_TOC_]]

## Credits

This program have been debelopped by Rémi Marchal, Boris Le Guennic et Kevin Bernot at the Institut des Sciences CHimiques de Rennes (ISCR UMR 6226).

## Aim of the program
This program aims at representing g-tensors extracted from MOLCAS calculations through cube files. 

## System requirement
The program is written in python3 and fortran.

To use it, the minimal requirements are:
<ul>
        <li>Linux/MacOs operating system (will not work on windows)</li>
        <li>A python3 interpreter</li>
        <li>The numpy module</li>
	<li>A fortran compiler</li>
</ul>

## Installation
<ol>
<li> <b>Fortran compilation</b>

The only requirement is to compile the cubegenerator.f90 fortran program.

For example:

gfortran -o cubegenerator cubegenerator.f90
</li>

<li><b>Environnement variable</b>

You should create an "ELIPSOID" environnement variable and point it to the location of the source file.

For example:

export ELIPSOID=<i> < Place where you installed the source files > </i>
</li>

## How to use the program
<ol>
        <li>go to the directory where you have your output file</li>
        <li>star the python script</li>
        <li>answer the questions asked bu the script</li>
</ol>
