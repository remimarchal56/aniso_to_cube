import numpy as np
from numpy.linalg import solve
import os

def coord(out,data):
      res=[]
      f=os.popen("grep -A1000 'Cartesian Coordinates / Bohr, Angstrom' "+out).readlines()
      trans = centroid(out,data)
      for i in range(4,len(f)):
            if len(str.split(f[i]))==0:
                  break
            symbole=[]
            pos=0
            for j in range(0,len(str.split(f[i])[1])):
                  # print(str.split(f[i])[1][j],int(str.split(f[i])[1][j]))
                  try :
                        int(str.split(f[i])[1][j])
                        pos=j
                        break
                  except:
                        pass
            symbole=str.split(f[i])[1][:pos].lower()
            # print(symbole)
            res.append([data[symbole],float(str.split(f[i])[5])-trans[0],float(str.split(f[i])[6])-trans[1],float(str.split(f[i])[7])-trans[2]])
      return(res)

def centroid(out,data):
    res =[]
    f=os.popen("grep 'Orbital angular momentum around' "+out).read()
    f=f.replace('(',' ')
    f=f.split()
    for i in range(4,7):
        print(f[i])
        res.append(float(f[i]))#*0.529177)
    print('ANGMOM',res)
    return(res)


def atom_list():
	f=os.popen("cat $ELIPSOID/atom-list").readlines()
	data={}
	nelem=int(str.split(f[0])[0])
	for i in range(0,nelem):
		data[str.split(f[i+1])[0].lower()]=str.split(f[i+1])[1]
	
	return(data)

data  = atom_list()
print("Molcas output files")
out=input("")

cini=coord(out,data)
print("Number of points for the cube grid (in each direction)")
npoints2=int(input())
npoints2=int(npoints2/6)*6
npoints=96

mult={}
f=os.popen("grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET' "+out).readlines()
multi=[]
for i in range(0,len(f)):
    multi.append(str.split(f[i])[8])
    

f=os.popen("grep 'gX =' "+out).readlines()
for i in range(0,len(multi)):
      ligne=str.split(f[i])
      mult[multi[i]]={}
      mult[multi[i]]["gX"]=float(ligne[2])*1.88973
      dist=(float(ligne[6])**2+float(ligne[7])**2+float(ligne[8])**2)**0.5
      mult[multi[i]]["xtensor"]=[float(ligne[6])/dist,float(ligne[7])/dist,float(ligne[8])/dist]

f=os.popen("grep 'gY =' "+out).readlines()
for i in range(0,len(multi)):
      ligne=str.split(f[i])
      
      mult[multi[i]]["gY"]=float(ligne[2])*1.88973
      dist=(float(ligne[6])**2+float(ligne[7])**2+float(ligne[8])**2)**0.5
      mult[multi[i]]["ytensor"]=[float(ligne[6])/dist,float(ligne[7])/dist,float(ligne[8])/dist]

f=os.popen("grep 'gZ =' "+out).readlines()
for i in range(0,len(multi)):
      ligne=str.split(f[i])
      
      mult[multi[i]]["gZ"]=float(ligne[2])*1.88973
      dist=(float(ligne[6])**2+float(ligne[7])**2+float(ligne[8])**2)**0.5
      mult[multi[i]]["ztensor"]=[float(ligne[6])/dist,float(ligne[7])/dist,float(ligne[8])/dist]

ecriture2=open('input-fortran',"w")
ecriture2.write(out+"_multiplet"+"\n")
ecriture2.write(str(npoints)+" \n")
ecriture2.write(str(len(mult.keys()))+" \n")

for i in mult.keys():
        normalised=np.zeros((3,3))
        dista=np.zeros(3)
        dista[0]=((mult[i]["xtensor"][0]*mult[i]["gX"])**2+(mult[i]["xtensor"][1]*mult[i]["gX"])**2+(mult[i]["xtensor"][2]*mult[i]["gX"])**2)**0.5
        dista[1]=((mult[i]["ytensor"][0]*mult[i]["gY"])**2+(mult[i]["ytensor"][1]*mult[i]["gY"])**2+(mult[i]["ytensor"][2]*mult[i]["gY"])**2)**0.5
        dista[2]=((mult[i]["ztensor"][0]*mult[i]["gZ"])**2+(mult[i]["ztensor"][1]*mult[i]["gZ"])**2+(mult[i]["ztensor"][2]*mult[i]["gZ"])**2)**0.5

      #   rotation=[mult[i]["xtensor"],mult[i]["ytensor"],mult[i]["ztensor"]]
        rotation=np.zeros((3,3))
        for j in range(0,3):
            rotation[j][0]=mult[i]["xtensor"][j]*mult[i]["gX"]/(dista[0])
            rotation[j][1]=mult[i]["ytensor"][j]*mult[i]["gY"]/(dista[1])
            rotation[j][2]=mult[i]["ztensor"][j]*mult[i]["gZ"]/(dista[2])
       # print("rotation",rotation)
        cfinal=[]
        for j in range(0,len(cini)):
            
      #       c2=[cini[j][1],cini[j][2],cini[j][3]]
      #       c3=np.matmul(rotation,c2)
      #       cfinal.append([cini[j][0],c3[0]*1.88973,c3[1]*1.88973,c3[2]*1.88973])
            cfinal.append([cini[j][0],cini[j][1]*1.88973,cini[j][2]*1.88973,cini[j][3]*1.88973])
        a=mult[i]['gX']
        b=mult[i]['gY']
        c=mult[i]['gZ']
        debut=[-1.2*mult[i]['gX'],-1.2*mult[i]['gY'],-1.2*mult[i]['gZ']]
        
        pas=[-2*debut[0]/npoints,-2*debut[1]/npoints,-2*debut[2]/npoints]
        rotation2=np.linalg.inv(rotation)
        ecriture=open(str(out)+"_multiplet"+str(i),"w")
        ecrire=""
        ecrire=ecrire+"MULTIPLET "+i+" \n"
        ecrire=ecrire+"MULTIPLET "+i+" \n"
        debut2=np.zeros(3)
        debut2[0]=debut[0]
        debut2[1]=debut[1]
        debut2[2]=debut[2]
        debut3=np.matmul(rotation,debut2)
        ecrire=ecrire+str(-1*len(cini))+" "+str(debut3[0])+" "+str(debut3[1])+" "+str(debut3[2])+" "+"1 \n"

        #ecrire=ecrire+str(npoints)+" "+str(pas[0])+" 0.0 0.0 \n"
        #ecrire=ecrire+str(npoints)+" 0.0 "+str(pas[1])+" 0.0 \n"
        #ecrire=ecrire+str(npoints)+" 0.0 0.0 "+str(pas[2])+" \n"
        debut3=np.matmul(rotation,[pas[0],0.0,0.0])
        ecrire=ecrire+str(npoints)+" "+str(debut3[0])+" "+str(debut3[1])+" "+ str(debut3[2])+" \n"
        debut3=np.matmul(rotation,[0.0,pas[1],0.0])
        ecrire=ecrire+str(npoints)+" "+str(debut3[0])+" "+str(debut3[1])+" "+ str(debut3[2])+" \n"
        debut3=np.matmul(rotation,[0.0,0.0,pas[2]])
        ecrire=ecrire+str(npoints)+" "+str(debut3[0])+" "+str(debut3[1])+" "+ str(debut3[2])+" \n"
        for j in range(0,len(cini)):
             
              ecrire=ecrire+" "+str(cfinal[j][0])+" "+str(float(cfinal[j][0]))+" "+str(cfinal[j][1])+" "+str(cfinal[j][2])+" "+str(cfinal[j][3])+" \n"
        ecrire=ecrire+" 1 1 \n"
        ecriture.write(ecrire)
        ecriture.close()
        
        ecriture2.write(i+"\n")
        ecriture2.write(str(a)+"\n")
        ecriture2.write(str(b)+"\n")
        ecriture2.write(str(c)+"\n")
        ecriture2.write(str(debut[0])+"\n")
        ecriture2.write(str(debut[1])+"\n")
        ecriture2.write(str(debut[2])+"\n")
        ecriture2.write(str(pas[0])+"\n")
        ecriture2.write(str(pas[1])+"\n")
        ecriture2.write(str(pas[2])+"\n")
        ecriture2.write(str(rotation2[0][0])+"\n")
        ecriture2.write(str(rotation2[0][1])+"\n")
        ecriture2.write(str(rotation2[0][2])+"\n")
        ecriture2.write(str(rotation2[1][0])+"\n")
        ecriture2.write(str(rotation2[1][1])+"\n")
        ecriture2.write(str(rotation2[1][2])+"\n")
        ecriture2.write(str(rotation2[2][0])+"\n")
        ecriture2.write(str(rotation2[2][1])+"\n")
        ecriture2.write(str(rotation2[2][2])+"\n")
ecriture2.close()

os.system("$ELIPSOID/cubegenerator")
os.system("rm input-fortran")        


