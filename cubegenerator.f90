program test

    implicit none

    integer :: i,npoints,nmult,j,k,l,m,npoints_rad
    integer :: ierr
    integer :: test2,posx,posy,posz
    character(len=70) :: file
    real, parameter :: pi = acos(-1.0)
    real :: x,y,z,rax,ray,raz,pas_rad,theta,phi
    real , dimension(6) :: z2
    character(len=100) :: filename2,filename
    character(len=5), allocatable :: nom(:)
    real, allocatable :: debut(:,:),pas(:,:),a(:),b(:),c(:),values(:,:,:)
    real, allocatable :: rotation(:,:)
    real, dimension(3) :: newxyz, newpos

    open(20,file='input-fortran',form="formatted",iostat=ierr,status="old")
    test2 = 0
    read(20,*)filename
    ! filename=trim(filename2)
    read(20,*)npoints
    read(20,*)nmult
    allocate(values(npoints,npoints,npoints))
    values=0.0
    posx=0
    posy=0
    posz=0
    allocate(nom(nmult))
    allocate(debut(nmult,3))
    allocate(pas(nmult,3))
    allocate(a(nmult))
    allocate(b(nmult))
    allocate(c(nmult))
    allocate(rotation(3,3))
    do i=1,nmult
        open(21,file='suite')
        read(20,*)nom(i)
        file=trim(filename)//trim(nom(i))//".cube"
        
        read(20,*)a(i)
        read(20,*)b(i)
        read(20,*)c(i)
        do j=1,3
            read(20,*)debut(i,j)
        end do
        do j=1,3
            read(20,*)pas(i,j)
        end do
        do j=1,3
            do k=1,3
                read(20,*)rotation(j,k)
            end do
        end do
       ! write(*,*)i,"pas",pas(i,:),npoints,"debut",debut(i,:)
        npoints_rad=2*(npoints**1.5)
        pas_rad=2.0*pi/npoints_rad
        theta=0.0
        
        do j=1,npoints_rad
            phi=0.0
            do k=1,npoints_rad
                ! newxyz(1) = a(i) * sin(theta) * cos(phi)
                ! newxyz(2) = b(i) * sin(theta) * sin(phi)
                ! newxyz(3) = c(i) * cos(theta)
                ! newpos = matmul(rotation,newxyz)
                ! x=newpos(1)
                ! y=newpos(2)
                ! z=newpos(3)
                ! posx=(x-debut(i,1))/pas(i,1)
                ! posy=(y-debut(i,2))/pas(i,2)
                ! posz=(z-debut(i,3))/pas(i,3)
                ! values(posx,posy,posz)=0.25
                x = a(i) * sin(theta) * cos(phi)
                y = b(i) * sin(theta) * sin(phi)
                z = c(i) * cos(theta)
                posx=(x-debut(i,1))/pas(i,1)
                posy=(y-debut(i,2))/pas(i,2)
                posz=(z-debut(i,3))/pas(i,3)
                values(posx,posy,posz)=0.25
                phi=phi+pas_rad
            end do
            theta=theta+pas_rad
        end do
        
        write(21,"(F7.3,F7.3,F7.3,F7.3,F7.3,F7.3)")values
        ! do j=1,npoints
        !     write(*,"(F5.1)")values(0,0,j)
        ! end do
        
        close(21)
        ! write(*,*)trim(filename)//trim(nom(i)),filename,nom(i)
        call system("cat "//trim(filename)//trim(nom(i))//" suite > "//file)
        call system("rm suite "//trim(filename)//trim(nom(i)))
    end do
    close(20)
    

end program
